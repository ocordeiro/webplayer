import Home from './components/Home.vue';
import Login from './components/Login.vue';
import Settings from './components/Settings.vue';
import Movies from './components/Movies.vue';
import Series from './components/Series.vue';
import Television from './components/Television.vue';
import Player from './components/Player.vue';

const routes = [
    { path: '/', component: Home },
    { path: '/login', component: Login },
    { path: '/settings', component: Settings },
    { path: '/filmes', component: Movies },
    { path: '/filmes/:category_id', component: Movies, props: true },
    { path: '/filme/:movie_id', component: Movies, props: true },
    { path: '/series', component: Series },
    { path: '/serie/:serie_id', component: Series, props: true },
    { path: '/serie/:serie_id/:season_id', component: Series, props: true },
    { path: '/serie/:serie_id/:season_id/:episode_id', component: Series, props: true },
    { path: '/televisao', component: Television },
    { path: '/televisao/:category_id', component: Television, props: true  },
    { path: '/televisao/:category_id/:channel_id', component: Television, props: true  },
    { path: '/assistir/:type/:stream_id', component: Player, props: true  },

];

export default routes;