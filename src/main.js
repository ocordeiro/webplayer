import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router';
import VueLazyload from 'vue-lazyload'

Vue.config.productionTip = false;

import routes from './routes';
import moment from 'moment';

Vue.use(VueRouter);
Vue.use(VueLazyload);

const router = new VueRouter({
    linkExactActiveClass: 'active',
    routes: routes
});


Vue.filter('base64decode', function (value) {
    return new TextDecoder().decode(Uint8Array.from(atob(value), c => c.charCodeAt(0)));
});

Vue.filter('isoDateToHour', function (value) {
    return new moment(value).format('HH:mm');
});

Vue.filter('isoDateFormat', function (value, format) {
    return new moment(value).format(format);
});

new Vue({
    router,
    render: h => h(App),
}).$mount('#app');
