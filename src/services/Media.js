
import UserService from './UserService';

export default class Api extends UserService{

    constructor (options) {
        super(options);
    }

    pushToHistory(media){
        if(!localStorage.getItem('recent')){
            localStorage.setItem('recent', '[]');
        }

        let recent = JSON.parse(localStorage.getItem('recent')).slice(0,5);
        recent = recent.filter(function(el) { return el.stream_id !== media.stream_id; });
        recent.push(media);

        localStorage.setItem('recent', JSON.stringify(recent))
    }

    static lazyImage(src) {
        return {
            src: src,
            error: require('../assets/images/default-thumbnail.png'),
            loading: require('../assets/images/default-thumbnail.png')
        }
    }

}