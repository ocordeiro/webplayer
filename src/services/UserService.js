import axios from "axios";

export default class UserService {

    constructor () {

        this.username = localStorage.getItem('username');
        this.password = localStorage.getItem('password');
        this.expiration = localStorage.getItem('expiration');

        this.BASE_URL = 'http://ver.nossa.tv:8880/';
        this.API_URL = `${this.BASE_URL}player_api.php?username=${this.username}&password=${this.password}`;
    }

    async login (username, password) {

        let response = await axios.get(`${this.BASE_URL}player_api.php?username=${username}&password=${password}`)

        localStorage.setItem('username', username);
        localStorage.setItem('password', password);
        localStorage.setItem('expiration', response.data.user_info.exp_date);
    }

    isLogged() {
        return !((this.username == null) || (this.password == null));
    }




}