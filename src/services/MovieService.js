
import Api from "./Media"
import axios from 'axios'

export default class MovieService extends Api {
    constructor (options) {
        super(options);

        this.categories = [];
        this.movies = [];

        this.movie = {};
        this.category = {}
    }

    async getCategories  () {
        if(!this.categories.length) {
            let response = await axios.get(this.API_URL + '&action=get_vod_categories');
            this.categories = response.data;
        }

        return this.categories
    }

    async getMoviesByCategory (category_id) {
        if(this.category.category_id === category_id) {
            return this.movies;
        }

        this.category = {
            category_id: category_id
        };

        if(this.categories.length){
            this.category = this.categories.find(x => x.category_id === category_id);
        }

        let response = await axios.get(this.API_URL + '&action=get_vod_streams&category_id=' + category_id);
        this.movies = response.data;

        return this.movies;
    }

    async getMovieInfo(movie_id) {
        // get from memory
        if(this.movies.length){
            this.movie = this.movies.find(x => x.stream_id.toString() === movie_id);
        }

        let response = await axios.get(this.API_URL + '&action=get_vod_info&vod_id=' + movie_id);
        this.movie.info = response.data.info;
        this.movie.movie_data = response.data.movie_data;

        return this.movie;
    }

    getMedia () {
        console.log(this.movie)
        return {
            type: 'filme',
            stream_id: this.movie.movie_data.stream_id,
            extension: '.' + this.movie.movie_data.container_extension,
            name: this.movie.name,
            cover: this.movie.info.movie_image,
            image: this.movie.info.backdrop_path ? this.movie.info.backdrop_path : '',
            relasedata:  this.movie.info.relasedata,
            director: this.movie.info.director,
            plot:  this.movie.info.plot,
            cast:  this.movie.info.cast
        }
    }

}