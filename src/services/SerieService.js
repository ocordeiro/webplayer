
import Api from "./Media"
import axios from 'axios'
import moment from 'moment'

export default class SerieService extends Api {
    constructor (options) {
        super(options);

        this.series = [];
        this.seasons = [];
        this.episodes = [];

        this.serie = {};
        this.season = {};
        this.episode = {};
    }

    async getSeries  () {
        if(!this.series.length) {
            let response = await axios.get(this.API_URL + '&action=get_series');
            this.series = response.data;
        }

        return this.series
    }

    async getSerieInfo (serie_id) {
        if(this.serie.serie_id === serie_id) {
            return this.serie;
        }

        this.serie = {
            serie_id: serie_id
        };

        let response = await axios.get(this.API_URL + '&action=get_series_info&series_id=' + serie_id);
        Object.assign(this.serie, response.data.info);

        this.episodes = [];
        this.seasons = response.data.seasons;
        this.episodes = response.data.episodes;

        this.season = this.seasons[0];

        return this.serie;
    }

    async getEpisodeInfo(episode_id, season_number) {
        this.season = this.seasons.find(d => d.season_number.toString() === season_number.toString());
        this.episode = this.episodes[season_number].find(d => d.id.toString() === episode_id.toString());;
        return this.episode;
    }

    getMedia () {
        return {
            type: 'series',
            extension: '.' + this.episode.container_extension,
            stream_id: this.episode.id,
            name: this.episode.title,
            cover: this.season.cover || this.serie.cover,
            image: this.episode.info.backdrop_path || this.serie.backdrop_path[0],
            releasedate:
                    this.episode.info.releasedate && this.episode.info.releasedate.length
                    ? moment(this.episode.info.releasedate).format('DD/MM/Y')
                    : moment(this.serie.releaseDate).format('Y'),
            rating:
                    this.episode.info.rating && this.episode.info.rating.length
                    ? this.episode.info.rating : this.serie.rating,
            director:
                    this.episode.info.director && this.episode.info.director.length
                    ? this.episode.info.director
                    : this.serie.director,
            plot:
                    this.episode.info.plot && this.episode.info.plot.length
                    ? this.episode.info.plot
                    : this.serie.plot,
            cast:
                    this.episode.info.cast && this.episode.info.cast.length
                    ? this.episode.info.cast
                    : this.serie.cast
        }
    }

}