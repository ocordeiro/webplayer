
import Media from "./Media"
import axios from "axios";

export default class TelevisionService extends Media {
    constructor (options) {
        super(options);

        this.category = {};
        this.channel = {};

        this.channels = [];
        this.categories = [];
        this.schedule = [];
    }

    async getCategories  () {
        if(!this.categories.length) {
            let response = await axios.get(this.API_URL + '&action=get_live_categories');
            this.categories = response.data;
        }

        return this.categories
    }

    async getChannelInfo (channel_id){
        if(this.channel.stream_id === channel_id) {
            return this.channel;
        }

        this.channel = {
            stream_id: channel_id
        };

        if(this.channels.length){
            this.channel = this.channels.find(x => x.stream_id.toString() === channel_id);
        }

        let response = await axios.get(this.API_URL + '&action=get_short_epg&stream_id=' + channel_id)
        this.schedule = response.data.epg_listings;
        return this.schedule;
    }

    async getChannelsByCategory (category_id) {
        if(this.category.category_id === category_id) {
            return this.channels;
        }

        this.category = {
            category_id: category_id
        };

        if(this.categories.length){
            this.category = this.categories.find(x => x.category_id === category_id);
        }

        let response = await axios.get(this.API_URL + '&action=get_live_streams&category_id=' + category_id);
        this.channels = response.data;

        return this.channels;
    }

}