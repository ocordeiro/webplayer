
import Api from "./Media"

export default class PlayerService extends Api {
    constructor (options) {
        super(options);
    }

    getUrlStream  (stream_id, resource_type, extension) {

        switch (resource_type) {
            case 'filme':
                resource_type = 'movie';
                break;
            case 'televisao':
                resource_type = 'live';
        }

        if(!extension){
            extension = ''
        }

        this.categories = [];
        return `${this.BASE_URL}${resource_type}/${this.username}/${this.password}/${stream_id}${extension}`;
    }

}